package ServiceTest;

import Services.RegistrationsService;
import domain.Registrations;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.RegistrationsDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class RegistrationsServiceTest {
@Mock
    RegistrationsDAO registrationsDAO;

@InjectMocks
    RegistrationsService registrationsService;


@Test
    public void testGetAllRegistrations() throws NoRecordFoundException {
        List<Registrations> regi = new ArrayList<>();
        regi.add(new Registrations());
        when(registrationsDAO.findAllRegistrations()).thenReturn(regi);

        List<Registrations> result = registrationsService.takeAllRegistrations();
        verify(registrationsDAO, times(1)).findAllRegistrations();
        assertEquals(regi, result);
    }
@Test
    public void testNoRecordFoundException()  {
    Assertions.assertThrows(NoRecordFoundException.class, () -> {
    registrationsService.takeAllRegistrations();
    });
}

@Test
    public void testUpdateIntoRegistrations() {
    Registrations registrations = new Registrations();
    when(registrationsDAO.updateMembers(10, "Tom Feryn")).thenReturn(true);

    boolean result = registrationsService.updateIntoRegistrations(10, "Tom Feryn");
    Assertions.assertTrue(result);
    Mockito.verify(registrationsDAO, times(1)).updateMembers(10,"Tom Feryn");
}


}



