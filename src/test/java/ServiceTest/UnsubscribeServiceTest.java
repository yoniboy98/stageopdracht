package ServiceTest;

import Services.UnsubscribeService;
import domain.Unsubscribe;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.UnsubscribeDAO;

import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UnsubscribeServiceTest {

    @Mock
    UnsubscribeDAO unsubscribeDAO;

    @InjectMocks
    UnsubscribeService unsubscribeService;

    @Test
    public void testFindUnsubscriberById() throws NoRecordFoundException, IDOutOfBoundException {
        Unsubscribe unsubscribe = new Unsubscribe();
        Mockito.when(unsubscribeDAO.findUnsubscriberById(1)).thenReturn(unsubscribe);

        Unsubscribe result = unsubscribeService.findUnsubscribeByID(1);
        Assertions.assertEquals(unsubscribe, result);
        Mockito.verify(unsubscribeDAO, times(1)).findUnsubscriberById(1);
    }

    @Test
    public void testNoRecordFoundException()  {
        Assertions.assertThrows(NoRecordFoundException.class,() ->{
unsubscribeService.findUnsubscribeByID(1);
        });
    }
@Test
    public void testFindUnsubscribeByName()  {
        Unsubscribe unsubscribe = new Unsubscribe();
        Mockito.when(unsubscribeDAO.findUnsubscribeByNameAndShowDate("")).thenReturn(unsubscribe);

        Unsubscribe rs = unsubscribeService.findUnsubscribeByName("");
        Assertions.assertEquals(unsubscribe,rs);
        Mockito.verify(unsubscribeDAO,times(1)).findUnsubscribeByNameAndShowDate("");
}
@Test
    public void testIDOutOfBoundException() {
        Assertions.assertThrows(IDOutOfBoundException.class,() ->{
           unsubscribeService.findUnsubscribeByID(0);
        });
}

}

