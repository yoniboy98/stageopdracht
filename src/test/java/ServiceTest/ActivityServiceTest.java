package ServiceTest;

import Services.ActivityService;
import domain.Activity;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.ActivityDAO;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class ActivityServiceTest {
    @Mock
    private ActivityDAO activityDAO;
   @InjectMocks
   private ActivityService activityService;


    private Date startDate;
    private Date endDate;

    {
        try {
            startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-01");
            endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2050-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


        @Test
           public void testGetAllActivities() throws NoRecordFoundException {
       List<Activity> acti = new ArrayList<>();
       acti.add(new Activity());
           when(activityDAO.findAllActivities()).thenReturn(acti);

           List<Activity> acti1 = activityService.findAllActivities();
           Mockito.verify(activityDAO, times(1)).findAllActivities();
           assertEquals(acti, acti1);
}

@Test
    public void testNoRecordFoundException()   {
       Assertions.assertThrows(NoRecordFoundException.class, () -> {
           activityService.findAllActivities();
        });
    }
@Test
public void testFindActivitiesBetweenDates() throws NoRecordFoundException, SQLException {
    List<Activity> activities = new ArrayList<>();
    activities.add(new Activity());
    when(activityDAO.findActivitiesBetweenDate(startDate,endDate)).thenReturn(activities);

    List<Activity> activities1 = activityService.findActivitiesBetweenDates(startDate,endDate);
    verify(activityDAO,times(1)).findActivitiesBetweenDate(startDate,endDate);
    assertEquals(activities,activities1);
}

@Test
    public void testDateTimeException() {
        Assertions.assertThrows(DateTimeException.class, () ->{
           activityService.findActivitiesBetweenDates(endDate,startDate);
        });
}
}




