package RepositoryTest;

import domain.Registrations;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import repo.RegistrationsDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;


public class RegistrationsDaoTest {
    private Connection connection;

    @BeforeEach
    public void connection() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFindAllRegistrationsIsEmpty() {
        RegistrationsDAO registrationsDAO = new RegistrationsDAO();
        List<Registrations> result = registrationsDAO.findAllRegistrations();
        Assertions.assertFalse(result.isEmpty());

    }

    @Test
    public void testUpdateRegistration() {
        RegistrationsDAO dao = new RegistrationsDAO();
        dao.updateMembers(10, "Tom Feryn");


    }


    @Test
    public void testFindRegistration() {
        RegistrationsDAO dao = new RegistrationsDAO();
        List<Registrations> result =  dao.findAllRegistrations();
        Assertions.assertFalse(Boolean.parseBoolean(result.toString()), "Yoni Vindelinckx");



    }

    @Test
    public void testFindAllRegistration(){
        RegistrationsDAO dao = new RegistrationsDAO();
        List<Registrations> result = dao.findAllRegistrations();
        Assertions.assertEquals("[Registrations{id=1, name='Yoni Vindelinckx', age=21, membershipfee='5', startdate=2018-05-08}, Registrations{id=3, name='Tim Vancacenberge', age=19, membershipfee='6', startdate=2017-04-09}, Registrations{id=4, name='Maxime Knol', age=12, membershipfee='6', startdate=2015-04-01}, Registrations{id=5, name='Arne Wouters', age=15, membershipfee='6', startdate=2017-10-22}, Registrations{id=6, name='Bryan vdb', age=17, membershipfee='6', startdate=2017-10-07}, Registrations{id=7, name='Dries vgb', age=24, membershipfee='6', startdate=2019-01-01}, Registrations{id=8, name='Alexander koekelberg', age=22, membershipfee='6', startdate=2019-04-02}, Registrations{id=9, name='Arne Lens', age=25, membershipfee='6', startdate=2019-12-18}, Registrations{id=10, name='Tom Feryn', age=21, membershipfee='6', startdate=2018-11-12}]", result.toString());
}}