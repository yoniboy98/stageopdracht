package RepositoryTest;

import domain.Activity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import repo.ActivityDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActivityDaoTest {
    private Connection connection;
    private Date startDate;
    private Date endDate;

    {
        try {
            startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-01");
            endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2050-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @BeforeEach
    public void connection() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFindAllActivities() {
        ActivityDAO activityDAO = new ActivityDAO();
        List<Activity> result = activityDAO.findAllActivities();
        Assertions.assertFalse(result.isEmpty());

    }

    @Test
    public void testFindActivitiesBetweenDates() {
        ActivityDAO activityDAO = new ActivityDAO();
        List<Activity> result = activityDAO.findActivitiesBetweenDate(startDate, endDate);
        Assertions.assertFalse(result.isEmpty());


    }

    @Test
    public void testFindTheActivity() {

        ActivityDAO activityDAO = new ActivityDAO();
        List<Activity> result = activityDAO.findAllActivities();
        Assertions.assertFalse(result.contains(activityDAO));
    }

    }
