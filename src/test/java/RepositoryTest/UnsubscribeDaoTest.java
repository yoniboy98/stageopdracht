package RepositoryTest;

import domain.Unsubscribe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repo.UnsubscribeDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class UnsubscribeDaoTest {
    private Connection connection;

@BeforeEach
        public void connection() {

            try {
                connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    @Test
    public void testFindUnsubscribeByID(){
        UnsubscribeDAO dao = new UnsubscribeDAO();
        Unsubscribe result = dao.findUnsubscriberById(1);
        Assertions.assertEquals("Unsubscribe[id : 1\n" +
                " unsubscribename :'Jeroen Buelens\n" +
                " unsubscribedate :2018-07-22\n" +
                " reasonwhy :No time]", result.toString());
    }
@Test
  public void testFindUnsubscribeByNameAndShowDate() {
UnsubscribeDAO dao = new UnsubscribeDAO();
Unsubscribe rs = dao.findUnsubscribeByNameAndShowDate("Jeroen Buelens");
Assertions.assertEquals("Jeroen Buelens",rs.getUnsubscribename());
}



}

