package domain;

import java.util.Date;

public class Registrations {
    private int id;



    private String name;
    private int age;
    private String membershipfee;
    private Date startdate;


    public Registrations(int id, String name, int age, String membershipfee, Date startdate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.membershipfee = membershipfee;
        this.startdate = startdate;
    }

    public Registrations() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMembershipfee() {
        return membershipfee;
    }

    public void setMembershipfee(String membershipfee) {
        this.membershipfee = membershipfee;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }



    @Override
    public String toString() {
        return "Registrations{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", membershipfee='" + membershipfee + '\'' +
                ", startdate=" + startdate +
                '}';
    }}


