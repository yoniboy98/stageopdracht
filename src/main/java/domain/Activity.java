package domain;

import java.util.Date;

public class Activity {
private int id;
private String activityname;
private Date activitydate;

    public Activity(int id, String activityname, Date activitydate) {
        this.id = id;
        this.activityname = activityname;
        this.activitydate = activitydate;
    }

    public Activity() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivityname() {
        return activityname;
    }

    public void setActivityname(String activityname) {
        this.activityname = activityname;
    }

    public Date getActivitydate() {
        return activitydate;
    }

    public void setActivitydate(Date activitydate) {
        this.activitydate = activitydate;
    }


    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", activityname=" + activityname +
                ", activitydate=" + activitydate +
                '}';
    }}