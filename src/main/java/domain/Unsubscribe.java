package domain;

import java.util.Date;

public class Unsubscribe {
    private int id;
    private String unsubscribename;
    private Date unsubscribedate;
    private String reasonwhy;

    public Unsubscribe() {

    }


    public int getId() {
        return id;
    }

    public Unsubscribe(int id, String unsubscribename, Date unsubscribedate, String reasonwhy) {
        this.id = id;
        this.unsubscribename = unsubscribename;
        this.unsubscribedate = unsubscribedate;
        this.reasonwhy = reasonwhy;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnsubscribename() {
        return unsubscribename;
    }

    public void setUnsubscribename(String unsubscribename) {
        this.unsubscribename = unsubscribename;
    }

    public Date getUnsubscribedate() {
        return unsubscribedate;
    }

    public void setUnsubscribedate(Date unsubscribedate) {
        this.unsubscribedate = unsubscribedate;
    }

    public String getReasonwhy() {
        return reasonwhy;
    }

    public void setReasonwhy(String reasonwhy) {
        this.reasonwhy = reasonwhy;
    }

    @Override
    public String toString() {
        return "Unsubscribe[" +
                "id : " + id + "\n" +
                " unsubscribename :'" + unsubscribename + "\n" +
                " unsubscribedate :" + unsubscribedate + "\n" +
                " reasonwhy :" + reasonwhy +
                ']';
    }
}
