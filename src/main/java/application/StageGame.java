package application;

import Services.*;
import domain.Activity;
import domain.Registrations;
import domain.Unsubscribe;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.System.err;


public class StageGame {

    private Map<Integer, String> menuMap;
    private Scanner scanner;
    private ActivityService activityService;
    private RegistrationsService registrationsService;
    private UnsubscribeService unsubscribeService;
    private boolean goBack = true;

    public StageGame(ActivityService activityService, RegistrationsService registrationsService, UnsubscribeService unsubscribeService) {
        this.scanner = new Scanner(System.in);
        this.activityService = activityService;
        this.registrationsService = registrationsService;
        this.unsubscribeService = unsubscribeService;

        //Initialise menu
        this.menuMap = new HashMap<>();
        menuMap.put(0, "quit the game");
        menuMap.put(1, "Show all activities");
        menuMap.put(2, "show all activities between dates");
        menuMap.put(3, "show all registrations in the DB");
        menuMap.put(4, "update a new member");
        menuMap.put(5, "show unsubscribers by ID");
        menuMap.put(6, "show Unsubscribe date by name");


    }

    /**
     * Start het spel op en toont een klein startscherm.
     */

    public void startGame() {
        System.out.println("================= New app from Yoni! =================");
        System.out.println("Stageopdracht Yoni Vindelinckx ");
        System.out.println("All options");
        showMenu();
    }

    /**
     * Geeft het menu weer.
     */

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }

    /**
     * Biedt de mogelijkheid aan om het nummer van een optie in te geven.
     */

    private void chooseOption() throws NumberFormatException {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.print("Choose your option : ");
            int i = Integer.parseInt(scanner.nextLine());
            Options(i);
        } catch (InputMismatchException e) {
            err.println("No valid input  ");
            chooseOption();

        }
        catch (NoRecordFoundException | IDOutOfBoundException | ParseException e) {
            System.err.println("error message");
            chooseOption();


        }

    }

    /**
     * gebruikt de methode die je opgeeft bij input(integer).
     * @param option Laat de user kiezen tussen verschillende opties (integer).
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.
     * @throws  IDOutOfBoundException Moest ID geljk of kleiner zijn dan 0.
     * @throws ParseException Moest hij een datum niet juist omzetten.
     */

    private void Options(int option) throws NoRecordFoundException, IDOutOfBoundException, ParseException {
        switch (option) {
            case 0:
                closeOpdracht();
                goBack = false;
                break;
            case 1:
                showAllActivities();
                break;
            case 2:
                showAllActivitiesBetweenDates();
                break;
            case 3:
                showAllRegistrations();
                break;
            case 4:
                updateIntoRegistrations();

                break;
            case 5:
                showUnsubscribersById();

                break;
            case 6:
                showUnsubscribeDateByName();
               break;

        }
        if (goBack) {
            chooseOption();

        }
        scanner.close();
    }

    /**
     * sluit de game.
     */
    private void closeOpdracht() {
        System.out.println("Closing the game");
    }


    /**
     * Geeft alles weer van tabel activity.
     *
     * @throws NoRecordFoundException meegegeven door findAllActivities
     * @throws NullPointerException meegegeven door findAllActivities
     */
    private void showAllActivities() throws NoRecordFoundException, NullPointerException {

        List<Activity> acti = activityService.findAllActivities();
        System.out.println("---------------ALL ACTIVITIES---------------");
        for (domain.Activity activiteit : acti) {
            System.out.println(activiteit.getId() + " " + activiteit.getActivityname() + " " + activiteit.getActivitydate());
        }

    }

    /**
     * Laat alle activiteiten zien tussen de opgegeven datums.
     * @throws ParseException Moest hij een datum niet juist omzetten.
     */
    private void showAllActivitiesBetweenDates() throws ParseException {
        System.out.println(" Insert the dates in the following format: YYYY-MM-DD");
        Date startDate = askForDate();
        Date endDate = askForDate();

        try {
            List<Activity> activities = activityService.findActivitiesBetweenDates(startDate, endDate);
            for (Activity activity : activities) {
                System.out.println(activity.getId() + " -- " + activity.getActivityname() + " ----> " + activity.getActivitydate());
            }
            System.out.println(activities.size() + " activities registered between those two dates.");
        } catch ( NoRecordFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Geeft alle informatie weer bij de id die opgegeven werd.
     */
    private void showUnsubscribersById() {
        try {
            int id = GiveID();

            System.out.println("Showing unsubscriber with ID " + id);
            Unsubscribe sub = unsubscribeService.findUnsubscribeByID(id);
            System.out.println(sub.toString());
        } catch (Exception e) {
            e.getStackTrace();

        }
    }


    /**
     * Geeft een lijst weer van alle registrations.
     * @throws NoRecordFoundException meegegeven door takeAllRegistrations.
     */

    private void showAllRegistrations() throws NoRecordFoundException {
        List<Registrations> regi = registrationsService.takeAllRegistrations();
        System.out.println("---------------ALL REGISTRATIONS---------------");
        for (domain.Registrations registration : regi) {
            System.out.println("\n");
            System.out.println("ID" + "\t" + "name ");
            System.out.println(registration.getId() + " " + registration.getName() + " \n  " + "age:" + registration.getAge() + " " + "membership FEE :" + registration.getMembershipfee() + " " + " BEGINDATE: " + registration.getStartdate());

        }
    }


    /**
     * update een member in de database.
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.
     */

    private void updateIntoRegistrations() throws NoRecordFoundException {
        System.out.println("here are all members");
        registrationsService.takeAllRegistrations().forEach
                (System.out::println);
        askTheUser();

    }


    /**
     * Hier laat ik de user een id ingeven die dan de opgevraagde records weergeven.
     * @return 0
     */
    private int GiveID() {
        if (scanner == null) {
            scanner = new Scanner(System.in);

        }
        try {
            System.out.println("ID?");
            int number = Integer.parseInt(scanner.nextLine());
            if (number >= 5) {
                System.out.println("no records with this ID");
                return Integer.parseInt(null);

            }

            return number;
        } catch (InputMismatchException e) {
            err.println("");


            return 0;
        }
    }

    /**
     * vraagt achter input van de user.
     */
    private void updatingMember() {
        System.out.println("Give the member id");
        int memberid = scanner.nextInt();
        String membername = scanner.nextLine();
        System.out.println("Give the name of the person you want to insert.");
        membername = chooseName();

        if(registrationsService.updateIntoRegistrations(memberid, membername))
            System.out.println("update success");
        else
            System.out.println("update will not work!");

    }

    /**
     * vraagt de user of hij een registration wil updaten.
     */
    private void askTheUser() {
        System.out.println("Do you want to update a member?");
        scanner = new Scanner(System.in);
        String yes = scanner.nextLine();
        if (yes.equals("yes") || yes.equals("ja")) {
            updatingMember();
        } else if (yes.equals("no") || yes.equals("nee")) {
            showMenu();

        }
    }

    /**
     * Vraagt de gebruiker om een datum in het formaat YYYY-MM-DD in te geven.
     * @throws ParseException  Moest hij een datum niet juist omzetten.
     * @return date. return een date.
     */
    private Date askForDate() throws ParseException {
        System.out.print("Insert a date: ");
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(scanner.nextLine());
            return date;
        }



    /**
     * Geeft de unsubscriber datum weer die je opzoekt op naam.
     */
    private void showUnsubscribeDateByName() {
        try {

            String name = chooseName();
            System.out.println("Here is the unsubscriber date of the person " + name);
            Unsubscribe sub = unsubscribeService.findUnsubscribeByName(name);
            System.out.println(sub.getUnsubscribedate());
            System.out.println("do you want to see another unsubscriber date?");
            String again = scanner.nextLine();
            if (again.equals("yes")) {
                showUnsubscribeDateByName();
            } else if (again.equals("no")) {
                   showMenu();



            } else {
                System.out.println("no legit yes or no!");
            }
        } catch ( Exception e) {
            e.getMessage();
        }
    }

    /**
     * Vraagt om input voor een naam.
     *
     * @return null
     */
    private String chooseName() {
        if (scanner == null) {
            scanner = new Scanner(System.in);

        }
        try {
            System.out.println("name?");
            String name = scanner.nextLine();
            if (name.isEmpty()) {
                System.out.println("no records with this name");
            }
            return name;
        } catch (InputMismatchException e) {
            err.println("");

        }
        return null;
    }


}