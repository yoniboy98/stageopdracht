package exceptions;

public class NoRecordFoundException extends Exception{
    public NoRecordFoundException(String s) {
        super("no records found");
    }


}
