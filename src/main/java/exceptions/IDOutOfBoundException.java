package exceptions;

public class IDOutOfBoundException extends Exception {
    public IDOutOfBoundException (String s) {
        super("No legit ID");
    }
}
