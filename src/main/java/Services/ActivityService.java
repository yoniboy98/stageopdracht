package Services;

import domain.Activity;
import exceptions.NoRecordFoundException;
import repo.ActivityDAO;

import java.sql.SQLException;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityService {

    private ActivityDAO activityDAO;

    public ActivityService(ActivityDAO activitydao) {
        this.activityDAO = activitydao;
    }

    /**
     * Zoekt naar alle activiteiten.
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.
     * @return activities
     */
    public List<Activity> findAllActivities() throws NoRecordFoundException {
        List<Activity> activities = activityDAO.findAllActivities();
        if (!activities.isEmpty()) {
            return activities;
        } else {
            throw new NoRecordFoundException("No records found ");
        }
    }

    /**
     * Zoekt naar alle activiteiten tussen de gegeven datums van de user.
     * @param startDate 1ste begindatum input
     * @param endDate 2de einddatum input
     * @return activities
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.

     */
    public List<Activity> findActivitiesBetweenDates(Date startDate, Date endDate) throws NoRecordFoundException{
        List<Activity> activities;
        if(endDate.compareTo(startDate) <0){
            throw new DateTimeException("The end date cannot be a date before the start date.");
        } else {
            activities = activityDAO.findActivitiesBetweenDate(startDate, endDate);
            if(activities.isEmpty()){
                throw new NoRecordFoundException("no records found");
            }
        }
        return activities;
    }


}
