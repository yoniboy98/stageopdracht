package Services;

import domain.Unsubscribe;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.UnsubscribeDAO;

import java.sql.Date;

public class UnsubscribeService {
    private UnsubscribeDAO unsubscribeDAO;
    public UnsubscribeService(UnsubscribeDAO unsubscribedao) {
        this.unsubscribeDAO = unsubscribedao;
    }

    /**
     * Zoekt naar een unsubscriber met bijhorende ID.
     * @param id Zoekt unsubscriber op ID.
     * @return unsubscribe
     * @throws IDOutOfBoundException Moest ID geljk of kleiner zijn dan 0.
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.
     */
    public Unsubscribe findUnsubscribeByID(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException("no legit ID!");
        } else {
            Unsubscribe unsubscribe = unsubscribeDAO.findUnsubscriberById(id);
            if (unsubscribe == null) {
                throw new NoRecordFoundException(" no unsubscribers ");
            } else {

return unsubscribe;
            }}}

    /**
     * Zoekt naar een unsubscriber op naam.
     * @param name Zoekt unsubscriber op naam
     * @return unsubscribe Zoekt naar een unsubscriber op naam.
     * @throws NullPointerException no unsubscriber found
     */
            public Unsubscribe findUnsubscribeByName(String name ) throws NullPointerException {
                if (name == null)
                    throw new NullPointerException("no name found");
             else {
                    Unsubscribe unsubscribe = unsubscribeDAO.findUnsubscribeByNameAndShowDate(name);
                    if (unsubscribe==null) {
                        throw new NullPointerException("no unsubscriber found");

                    } else {
                        return unsubscribe;
                    }
                }
                }}

