package Services;

import domain.Registrations;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.RegistrationsDAO;

import java.util.ArrayList;
import java.util.List;

public class RegistrationsService {
    private RegistrationsDAO registrationsDAO;

    public RegistrationsService(RegistrationsDAO registrationsdao) {
        this.registrationsDAO = registrationsdao;
    }

    /**
     * Zoekt naar alle registrations.
     * @return registrations
     * @throws NoRecordFoundException Moest er geen data opgehaald kunnen worden.
     */
    public List<Registrations> takeAllRegistrations() throws NoRecordFoundException {
        List<Registrations> registrations = registrationsDAO.findAllRegistrations();
        if (!registrations.isEmpty()) {
            return registrations;
        } else {
            throw new NoRecordFoundException("no records found");
        }


    }

    /**
     * update een bestaande registratie.
     * @param memberid id van de member
     * @param membername naam van de member
     * @throws NullPointerException Moest mermbername gelijk zijn aan null.

     * @return false
     * @throws NullPointerException Moest membername gelijk zijn aan null.
     */
    public boolean updateIntoRegistrations(int memberid, String membername) throws NullPointerException
    {
        if (membername == null) {
        throw new NullPointerException();
        } else {
            if(registrationsDAO.updateMembers(memberid,membername))
                return true;
            else
                return false;
        }
    }

}



