import Services.ActivityService;
import Services.RegistrationsService;
import Services.UnsubscribeService;
import application.StageGame;
import repo.ActivityDAO;
import repo.RegistrationsDAO;
import repo.UnsubscribeDAO;

public class ExcecuteApp {


    /**
     * Main class waar alles in execute zal worden.
     * @param args  agrument.
     */
    public static void main(String[] args) {
        ActivityDAO activitydao = new ActivityDAO();
        ActivityService activityService = new ActivityService(activitydao);
        RegistrationsDAO registrationsdao = new RegistrationsDAO();
        RegistrationsService registrationsService = new RegistrationsService(registrationsdao);
        UnsubscribeDAO unsubscribedao = new UnsubscribeDAO();
        UnsubscribeService unsubscribeService = new UnsubscribeService(unsubscribedao);

        StageGame stage = new StageGame(activityService, registrationsService, unsubscribeService);
        stage.startGame();

    }}

