package repo;

import domain.Registrations;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RegistrationsDAO {
    /**
     * Zoekt naar alle inschrijvingen en al zijn kolommen.
     * @return registrations
     */
    public List<Registrations> findAllRegistrations() {
        List<Registrations> registrations = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement ps = con.prepareStatement("SELECT * FROM registrations");
            ps.execute();
            ResultSet result = ps.getResultSet();
            while (result.next()) {
                Registrations registrations1 = new Registrations();
                registrations1.setId(result.getInt("id"));
                registrations1.setName(result.getString("name"));
                registrations1.setAge(result.getInt("age"));
                registrations1.setMembershipfee(result.getString("membership_fee"));
                registrations1.setStartdate(result.getDate("startdate"));
                registrations.add(registrations1);
            } con.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } return registrations;

    }


    /**
     * update een member.
     * @param memberid id van de member
     * @param membername naam van de member
     * @return false boolean conditie.
     */
    public boolean updateMembers(int memberid, String membername) {
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL,DataBaseUtil.USERNAME,"");
            PreparedStatement statement = con.prepareStatement("UPDATE `jeugdhuis`.`registrations` SET `id` = ?, `name` = ?, `age` = ? WHERE `id` = ? ");
            statement.setInt(1, memberid);
            statement.setString(2,membername);
         statement.setInt(3,21);
         statement.setInt(4,memberid);

            if(statement.executeUpdate() > 0){
                con.close();
                statement.close();
                return true;
            } else {
                con.close();
                statement.close();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}


