package repo;

import domain.Activity;
import utilities.DataBaseUtil;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ActivityDAO {

    /**
     * Zoekt in tabel activiteiten naar alle kolommen.
     * @return act Is de arraylist die je return.
     */
    public List<Activity> findAllActivities() {
        List<Activity> act = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement ps = con.prepareStatement("SELECT * FROM activity");
            ps.execute();
            ResultSet result = ps.getResultSet();
            while(result.next()) {
                Activity activity = new Activity();
                activity.setId(result.getInt("id"));
                activity.setActivityname(result.getString("activity_name"));
                activity.setActivitydate(result.getDate("activity_date"));
                act.add(activity);
            } con.close();
            ps.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return act;
    }

    /**
     *
     * @param startDate startdatum input
     * @param endDate einddatum intput
     * zoekt naar alle activiteiten binnen de gegeven datums door de user.
     * @return activities geeft alle activiteiten weer die bij die input horen.

     */
    public List<Activity> findActivitiesBetweenDate(java.util.Date startDate, java.util.Date endDate)  {
        List<Activity> activities = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL,DataBaseUtil.USERNAME,"");
PreparedStatement ps = con.prepareStatement("SELECT id, activity_name, activity_date FROM jeugdhuis.activity WHERE activity_date BETWEEN ? AND ?");


            SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd");
            String startDateForSQL = format.format(startDate);
            String endDateForSQL = format.format(endDate);
            ps.setDate(1, Date.valueOf(startDateForSQL));
           ps.setDate(2, Date.valueOf(endDateForSQL));
ps.execute();
            ResultSet rs = ps.getResultSet();

            while(rs.next()){
                Activity activity = new Activity();

                activity.setId(rs.getInt("id"));
                activity.setActivityname(rs.getString("activity_name"));
                activity.setActivitydate(rs.getDate("activity_date"));

                activities.add(activity);
            }
            con.close();
          ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activities;
    }


}



