package repo;

import domain.Unsubscribe;
import exceptions.NoRecordFoundException;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.Calendar;

public class UnsubscribeDAO {
    /**
     * zoekt naar de unsubscriber met bijhorende ID.
     *
     * @param id Zoekt naar unsubscriber op ID.
     * @return unsubscribe
     */
    public Unsubscribe findUnsubscriberById(int id) {
        Unsubscribe unsubscribe = null;
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Unsubscribe WHERE id = ?");

            ps.setInt(1, id);
            ps.execute();
            ResultSet results = ps.getResultSet();

            while (results.next()) {
                unsubscribe = new domain.Unsubscribe();
                unsubscribe.setId(results.getInt("id"));
                unsubscribe.setUnsubscribename(results.getString("unsubscribe_name"));
                unsubscribe.setUnsubscribedate(results.getDate("unsubscribe_date"));
                unsubscribe.setReasonwhy(results.getString("reason_why"));


            }
            con.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return unsubscribe;
    }

    /**
     * @param name naam string
     * Zoekt naar unsubscriber op naam om daarna hun unsubscribe datum weer te geven.
     * @return unsubscribe
     */
    public Unsubscribe findUnsubscribeByNameAndShowDate(String name) {
        Unsubscribe unsubscribe = null;
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement ps = con.prepareStatement("SELECT id, unsubscribe_name , unsubscribe_date, reason_why FROM `jeugdhuis`.`unsubscribe` WHERE unsubscribe_name = ? ");
            ps.setString(1, name);
            ps.execute();

            ResultSet rs = ps.getResultSet();

            while (rs.next()) {
                unsubscribe = new domain.Unsubscribe();
                unsubscribe.setId(rs.getInt("id"));
                unsubscribe.setUnsubscribename(rs.getString("unsubscribe_name"));
                unsubscribe.setUnsubscribedate(rs.getDate("unsubscribe_date"));
                unsubscribe.setReasonwhy(rs.getString("reason_why"));

            }
            con.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return unsubscribe;
    }

    }
