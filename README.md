# StageOpdracht 

StageOpdracht
Dit is een stageopdracht die we kregen tussen 9 oktober en 14 oktober.
Je begint met een menu en er zijn 6 opties die verschillende zaken verrichten.


# Installatie

Stap 1: installeer de databank
Het programma hangt af van de 'jeugdhuis' databank. Daarom is het vereist om deze eerst installeren. Dit kan gedaan worden door het games-mySQL.sql bestand uit te voeren
Als de databank nog nooit gemaakt werd, verwijder dan het volgende lijntje uit het games-mySQL.sql bestand:
drop database games;

Stap 2: project openen in je IDE
Open het project in je IDE. Als het project geopend werd, klik dan in de project tree op het 'pom.xml' bestand.
Ga vervolgens naar het 'maven' menu en bij de lifecycle van het project selecteer je 'clean' en 'install' en executeer.
